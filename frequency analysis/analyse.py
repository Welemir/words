from collections import Counter
from string import punctuation

import pandas as pd

PUNCTUATION = f'—{punctuation}–«»'
IGNORED = ('а', 'ай', 'ах', 'б', 'ба', 'без', 'бы', 'в', 'во', 'г','де', 'для', 'до', 'ж', 'же', 'за', 'и', 'из', 'или',
           'к', 'ка', 'ко', 'ли', 'либо', 'на', 'над', 'не', 'ни', 'но', 'ну', 'о', 'об', 'ой', 'от', 'перед', 'по',
           'под', 'при', 'про', 'с', 'со', 'те', 'то', 'ту', 'тю', 'у', 'уж', 'уй', 'фу', 'через', 'эй', 'эх')


def file_lines(path: str) -> str:
    """
    Генератор, последовательно возвращает все непустые строки из файла, имя которого принимает в аргументе
    :param path: путь к файлу
    """
    with open(path) as file:
        for line in file:
            if line.rstrip():
                yield line.rstrip()


def words(line: str) -> str:
    """
    Генератор, поставляет отдельные слова из строки, разбивая ее по пробелу,  числа (самостоятельные лексемы,
    но не часть слова - например имени робота) и слова из кортежа IGNORED игнорируются
    :param line: строка для разбиения
    """
    for word in line.split():
        if word not in IGNORED  and not word.isdigit():
            yield word


def clear_line(line: str) -> str:
    """
    Функция убирает из строки знаки пунктуации и приводит к нижнему регистру
    :param line: строка
    :return: преобразованную строку
    """
    return ''.join((e.lower() for e in line if e not in PUNCTUATION))


def perform_analyse(path: str) -> Counter:
    """
    Функция анализа частоты слов, использует встроенный класс Counter
    :param path:  путь к файлу txt
    :return:  Counter, объект с готовыми данными
    """
    c = Counter()
    for line in map(words, map(clear_line, (file_lines(path)))):
        c.update(line)
    return c


def save_excel(name: str, a_list: list):
    """
    Функция сохраняет полученный список кортежей (по-умолчанию генерируется методом most_common() класса
    Counter) в эксель файл, сами слова будут индексами, а их частота колонкой.
    :param name: имя файла для сохранения, например holy_bible.xlsx
    :param a_list: список кортежей
    :return: None
    """
    sorted_by_values = dict(a_list)
    df = pd.DataFrame(index=sorted_by_values.keys())
    df.index.name = 'Слово'
    df['Частота'] = pd.Series(sorted_by_values)
    df.to_excel(name)


if __name__ == '__main__':
    file_path = input("Введите путь к файлу:")
    result = perform_analyse(file_path)
    print("Анализ завершен, сохранение...")
    save_excel('result.xlsx', result.most_common())
    print('Сохранено')
